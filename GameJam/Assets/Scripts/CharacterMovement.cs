﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class CharacterMovement : MonoBehaviour
{
    public bool debug = false;              // If debug information is displayed
    public GameObject startingWaypoint;     // The starting waypoint
    public Vector3 startingPosition;        // The starting position
    public float acceleration = 6;          // The acceleration
    public float deceleration = 10;         // The deceleration
    public float maxSpeed = 1;              // The max speed
    public float stopDistance = 0.1f;       // The stop distance

    private Vector3 heading;                // The heading
    private bool arrived = true;            // If he character has arrived
    private Rigidbody rigidBody;            // The rigidbody

    private bool validStartPosition;        // If the character starts at a waypoint or not

    /**
     * Initializer: Checks if the character starts at a wapoint and if it does moves it to that waypoint
     */
    void Start()
    {
        if (!startingWaypoint)
        {
            heading = transform.position;
            validStartPosition = false;
            if (debug) Debug.Log("No starting waypoint for " + name);
        }
        else
        {
            if (startingPosition == new Vector3())
            {
                transform.position = startingWaypoint.transform.position;
                if (debug) Debug.Log("Starting at waypoint " + name);
            }
            else
            {
                transform.position = startingWaypoint.transform.position;
                if (debug) Debug.Log("Starting at position with waypoint " + name);
            }
            heading = startingWaypoint.transform.position;
            validStartPosition = true;
        }

        rigidBody = GetComponent<Rigidbody>();
    }

    /*
     * If the player is not at its heading then moves it to that position
     */
    void Update()
    {
        Vector3 relativePos = (heading - transform.position);
        if (!arrived)
        {
            float DistanceToTarget = relativePos.magnitude;

            if (DistanceToTarget <= stopDistance)
            {
                arrived = true;

            }
            else
            {
                rigidBody.AddForce(relativePos.normalized * acceleration * Time.deltaTime, ForceMode.VelocityChange);
            }
        }
        else
        {
            rigidBody.AddForce(relativePos.normalized * 5.4f * Time.deltaTime, ForceMode.VelocityChange);
        }

        ManageSpeed(deceleration, maxSpeed);
    }

    /**
     * Manages the characters speed
     * 
     * @param deceleration      The character's deceleration
     * @param maxSpeed          The character's max speed
     */
    public void ManageSpeed(float deceleration, float maxSpeed)
    {
        Vector3 currentSpeed = rigidBody.velocity;

        if (currentSpeed.magnitude > 0)
        {
            rigidBody.AddForce((currentSpeed * -1) * deceleration * Time.deltaTime, ForceMode.VelocityChange);
            if (rigidBody.velocity.magnitude > maxSpeed)
                rigidBody.AddForce((currentSpeed * -1) * deceleration * Time.deltaTime, ForceMode.VelocityChange);
        }
    }

    /*
     * Moves the character to a new location
     * 
     * @param destination   The location the player will travel to
     */
    public void MoveTo(Vector3 destination)
    {
        if (arrived)
        {
            heading = destination;
            arrived = false;
        }
    }

    /**
     * Checks if the character is moving
     * 
     * @return arrived      If the character is moving or not
     */
    public bool IsMoving
    {
        get { return !arrived; }
    }

    /**
     * Checks if the character jas a valid start position
     * 
     * @param validStartPosition      If the character has a valid start position or not
     */
    public bool HasValidStartPosition
    {
        get { return validStartPosition; }
    }
}
