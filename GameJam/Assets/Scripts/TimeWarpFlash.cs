﻿using UnityEngine;
using System.Collections;

public class TimeWarpFlash : MonoBehaviour 
{
    enum FlashState
    {
        Off,
        Up,
        Down
    }

    public Color colour = Color.white;  // The colour of the flash
    public float startAlpha = 0f;       // The alpha value at the start of the flash
    public float maxAlpha = 1f;         // The alpha value at the height of the flash
    public float upTime = 0.5f;         // The time to get to the height of the flash
    public float downTime = 0.5f;       // The time to get from the height to the end of the flash

    public bool flash = false;          // Flash trigger

    private Texture2D flashPixel;       // The pixel shown when the flash is triggered

    private Timer timer;                // Times the flashes

    private FlashState flashState;      // The state of the flash

    /**
     * Constructor: Initialises the variables and sets the flash to off
     */
	void Start () 
    {
        flashState = FlashState.Off;
        flashPixel = new Texture2D(1, 1);
        SetFlashAlpha(startAlpha);
	}

    /**
     * Updates the flash if it is running
     */
	void Update () 
    {
        if (flash)
        {
            switch (flashState)
            {
                case FlashState.Off:
                    StartFlash();
                    break;
                case FlashState.Up:
                    IncreaseFlashAlpha();
                    break;
                case FlashState.Down:
                    DecreaseFlashAlpha();
                    break;
            }
        }
	}

    /**
     * Draws the flash
     */
    public void DrawFlash()
    {
        if (flash)
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), flashPixel);
    }

    /**
     * Starts the flash
     */
    public void StartFlash()
    {
        flashState = FlashState.Up;
        timer = new Timer(upTime);
        flash = true;
    }

    /**
     * Increases the flash until the timer has run out
     */
    private void IncreaseFlashAlpha()
    {
        if (timer.TimeFinished())
        {
            flashState = FlashState.Down;
            timer.ResetTimer(downTime);
        }
        else
        {
            SetFlashAlpha(Mathf.Lerp(startAlpha, maxAlpha, timer.Elapsed));
        }
    }

    /**
     * Decreases the flash until the timer has run out
     */
    private void DecreaseFlashAlpha()
    {
        if (timer.TimeFinished())
        {
            flashState = FlashState.Off;
            SetFlashAlpha(0f);
            flash = false;
            timer = null;
        }
        else
        {
            SetFlashAlpha(Mathf.Lerp(maxAlpha, startAlpha, timer.Elapsed));
        }
    }

    /**
     * Sets the flash alpha level
     * 
     * @param alpha     The new alpha level
     */
    private void SetFlashAlpha(float alpha)
    {
        colour.a = alpha;
        flashPixel.SetPixel(0, 0, colour);
        flashPixel.Apply();
    }
}
