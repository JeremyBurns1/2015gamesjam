﻿using UnityEngine;
using System.Collections;

/*!
 * WayPoint: A point on the screen that contains paths connected to other points
 */
public class WayPoint : MonoBehaviour 
{
    public GameObject[] waypointPaths;      // The paths connected to the waypoint
    public bool showPathGizmos = false;     // If the gizmos are displayed

    private int currentPathIndex;           // The current selected path

    /**
     * Sets the current path index to zero
     */
    void Awake()
    {
        currentPathIndex = 0;
    }

    /**
     * Sets the index to the next path in the list and returns it
     * 
     * @return  path    The next path in the list
     */
    public GameObject SelectNextPath()
    {
        currentPathIndex++;

        if (currentPathIndex == waypointPaths.Length)
            currentPathIndex = 0;

        return waypointPaths[currentPathIndex];
    }

    /**
     * Sets the index to the previous path in the list and returns it
     * 
     * @return  path    The previous path in the list
     */
    public GameObject SelectPreviousPath()
    {
        currentPathIndex--;

        if (currentPathIndex == -1)
            currentPathIndex = waypointPaths.Length - 1;

        return waypointPaths[currentPathIndex];
    }

    /**
     * Gets the current path
     * 
     * @return  The current path
     */
    public GameObject CurrentPath
    {
        get { return waypointPaths[currentPathIndex]; }
    }

    /**
     * Gets the array of waypoints
     * 
     * @return  The array of waypoints
     */
    public GameObject[] WaypointPaths
    {
        get { return waypointPaths; }
    }

    /**
     * Draws the gizmos, cyan circle for waypoints and yellow line for paths
     */
    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(transform.position, .3f);

        if (showPathGizmos)
        {
            if (waypointPaths.Length > 0)
            {
                foreach (GameObject point in waypointPaths)
                {
                    if (point)
                    {
                        Gizmos.color = Color.yellow;
                        Gizmos.DrawLine(transform.position, point.transform.position);
                    }
                    else
                    {
                        Debug.LogError("Waypoint " + name + " missing path");
                    }
                }
            }
            else
            {
                Debug.LogError("Waypoint " + name + " needs at least one path");
            }
        }
    }
}
