﻿using UnityEngine;
using System.Collections;

public class DeathTimer : MonoBehaviour {
    public ParticleSystem[] deathParticles;     // The death particles
    public AudioClip deathAudio;                // The audio played when the player dies
	public AudioClip[] screams;

    private Timer timer;                        // A timer that times the delay to the player death
    private bool deathTriggered = false;        // If the player death is triggered

    /**
     * Checks if the death timer is running and if it has run out kills the player
     */
	void Update() 
    {
        if (timer)
        {
            if (!deathTriggered && timer.TimeFinished())
            {
                Kill();
                deathTriggered = true;
            }
        }
	}

    /**
     * Starts the death timer
     * 
     * @param timeTillDeath     The amount of time before the character dies
     */
    public void StartDeathTimer(float timeTillDeath)
    {
        timer = new Timer(timeTillDeath);
    }

    /**
     * Kills the character, activates the death particles, plays the death audio and destroys the player
     */
    private void Kill()
    {
        foreach (ParticleSystem particle in deathParticles)
        {
            Instantiate(particle, transform.position, transform.rotation);
            particle.Play();
        }

        if (deathAudio)
        {
            AudioSource.PlayClipAtPoint(deathAudio, transform.position);
        }

		if (screams.Length > 0)
		{
			int selected = UnityEngine.Random.Range(0, screams.Length - 1); // Select random sound to play
			gameObject.GetComponent<AudioSource>().PlayOneShot(screams[selected]); // play selected sound from array.
		}

        Destroy(gameObject, 0.1f);
    }
}
