﻿using UnityEngine;
using System.Collections;

public class CameraWobble : MonoBehaviour 
{
    // TODO: Play the shake when a player explodes
    // Make a wobble that (gradually) gets more intense the drunker the player gets 
    // Should also make the fog gradually get more intenense (instead of jumping)
    // Add a developer option to turn of camera affects for testing
    // Seperate the camera from the camera object so that the wobble still follows the player

    public bool debug = false; 
    public FollowPlayer follow;             // The follow player script

    private float shakeDuration = 0.5f;      // The shake duration
    private float shakeSpeed = 8f;           // The shake speed
    private float shakeMagnitude = 0.5f;     // The shake magnitude

    public float wobbleDuration = 20f;      // The wobble duration
    public float wobbleSpeed = 20f;         // The wobble speed
    public float wobbleMagnitude = 0.5f;    // The wobble magnitude
    
    public bool testWobble = false;

    private int wobbleLevel = 0;

	// Update is called once per frame
	void Update () 
    {
        if (testWobble)
        {
            testWobble = false;
            StartWobble();
        }

        if (wobbleLevel != GameStats.DrunkenessLevel) wobbleLevel = GameStats.DrunkenessLevel;
	}

    public void StartWobble()
    {
        StopAllCoroutines();
        StartCoroutine("Wobble");
    }

    public void StartShake()
    {
        StopAllCoroutines();
        StartCoroutine("Shake");
        if (debug) Debug.Log("Shake triggered");
    }

    IEnumerator Wobble()
    {
        float elapsed = 0.0f;

        float randomStart = Random.Range(-1000.0f, 1000.0f);

        while (true)
        {
            elapsed += Time.deltaTime;

            float percentComplete = elapsed / wobbleDuration;

            float wobble = percentComplete - (int) System.Math.Round(percentComplete, 0, System.MidpointRounding.AwayFromZero);

            if (wobbleLevel > 5) wobble = Random.Range(0f, 100.0f) / 100;

            // We want to reduce the shake from full power to 0 starting half way through
            float damper = 1.0f - Mathf.Clamp(2.0f * wobble - 1.0f, 0.0f, 1.0f);

            // Calculate the noise parameter starting randomly and going as fast as speed allows
            float alpha = randomStart + wobbleSpeed * percentComplete;

            // map noise to [-1, 1]
            float x = Mathf.PerlinNoise(alpha, 0.0f) * 2.0f - 1.0f;
            float z = Mathf.PerlinNoise(0.0f, alpha) * 2.0f - 1.0f;

            x *= wobbleMagnitude * damper;
            z *= wobbleMagnitude * damper;

            transform.position = new Vector3(follow.CameraPosition.x + x, follow.CameraPosition.y, follow.CameraPosition.z + z);

            Debug.Log("Percentage complete " + percentComplete);

            yield return null;
        }

        //transform.position = follow.CameraPosition;
    }

    IEnumerator Shake()
    {
        float elapsed = 0.0f;

        float randomStart = Random.Range(-1000.0f, 1000.0f);

        while (elapsed < shakeDuration)
        {
            elapsed += Time.deltaTime;

            float percentComplete = elapsed / shakeDuration;

            // We want to reduce the shake from full power to 0 starting half way through
            float damper = 1.0f - Mathf.Clamp(2.0f * percentComplete - 1.0f, 0.0f, 1.0f);

            // Calculate the noise parameter starting randomly and going as fast as speed allows
            float alpha = randomStart + shakeSpeed * percentComplete;

            // map noise to [-1, 1]
            float x = Mathf.PerlinNoise(alpha, 0.0f) * 2.0f - 1.0f;
            float z = Mathf.PerlinNoise(0.0f, alpha) * 2.0f - 1.0f;

            x *= shakeMagnitude * damper;
            z *= shakeMagnitude * damper;

            transform.position = new Vector3(follow.CameraPosition.x + x, follow.CameraPosition.y, follow.CameraPosition.z + z);

            yield return null;
        }

        transform.position = follow.CameraPosition;
    }
}
