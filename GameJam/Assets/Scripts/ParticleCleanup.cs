﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleCleanup : MonoBehaviour 
{
    /**
     * Starts the check is alive coroutime
     */
    void OnEnable()
    {
        StartCoroutine("CheckIfAlive");
    }

    /**
     * Checks if the the particle is still running and if it isn't, destroys it
     */
    IEnumerator CheckIfAlive()
    {
        ParticleSystem ps = this.GetComponent<ParticleSystem>();

        while (true && ps != null)
        {
            yield return new WaitForSeconds(0.5f);
            if (!ps.IsAlive(true))
            {
                GameObject.Destroy(this.gameObject);
                break;
            }
        }
    }
}
