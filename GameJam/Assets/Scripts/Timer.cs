﻿using UnityEngine;
using System.Collections;

public class Timer
{
    private float totalTime;        // The amount time
    private float startTime;        // The time the timer is triggered
    private bool running;           // If the timer is running or paused

    private float startPauseTime;   // The time the pause started
    private float totalPauseTime;   // The total time the timer was paused

    /**
     * Constructor: Creates a timer and starts it
     * 
     * @param countdownTime     The amount of time the timer is set to
     */
    public Timer(float countdownTime)
    {
        ResetTimer(countdownTime);
    }

    /**
     * Resets the timer at the previously set time
     */
    public void ResetTimer()
    {
        ResetTimer(totalTime);
    }

    /**
     * Resets the timer at a new time
     * 
     * @param countdownTime     The new time
     */
    public void ResetTimer(float countdownTime)
    {
        running = true;
        totalTime = countdownTime;
        startTime = UnityEngine.Time.realtimeSinceStartup;
        startPauseTime = 0f;
        totalPauseTime = 0f;
    }

    /**
     * Pause the timer
     */
    public void Pause()
    {
        if (running)
        {
            startPauseTime = UnityEngine.Time.realtimeSinceStartup;
            running = false;
        }
    }

    /**
     * Resume the timer
     */
    public void Resume()
    {
        if (!running)
        {
            totalPauseTime += UnityEngine.Time.realtimeSinceStartup - startPauseTime;
            running = true;
        }
    }

    /**
     * Checks if the timer is still running
     * 
     * @return bool     True if the timer has run out, false if not
     */
    public bool TimeFinished()
    {
        return UnityEngine.Time.realtimeSinceStartup > startTime + totalTime + totalPauseTime;
    }

    /**
     * Returns the amount of time elapsed
     * 
     * @return float    The time elapsed
     */
    public float Elapsed
    {
        get {   float result = UnityEngine.Time.realtimeSinceStartup - startTime- totalPauseTime;
                if (!running) result -= UnityEngine.Time.realtimeSinceStartup - startPauseTime;
                return result;
        }
    }

    /**
     * Implicity converts a Timer into a bool
     * 
     * @param timer     Then timer to convert
     * @return bool     True if the timer is initalized, false if not
     */
    public static implicit operator bool(Timer timer)
    {
        return timer != null;
    }
}
