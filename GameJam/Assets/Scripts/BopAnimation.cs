﻿using UnityEngine;
using System.Collections;

public class BopAnimation : MonoBehaviour 
{
    public float mass = 6f;             // The mass of the object
    public float force = 20f;           // The force applied to the object
    private float timeDelay = 10;       // The time delay between 'bops', this number isn't important because it resets when the object reaches zero
    private float velocity;             // The velocity, calculated by f = ma
    private Timer timer;                // Times the jumps

    /**
     * Initializer: Starts the timer and calculates the velcocity
     */
	void Start () 
    {
        timer = new Timer(timeDelay);
        velocity = force / mass;
	}

    /**
     * Calculates the new Y position and if the y position is less than zero it resets the tier and begins the hop again
     */
    void Update()
    {
        velocity = force / mass;

        if (timer.TimeFinished() || transform.localPosition.y < 0)
        {
            timer.ResetTimer(timeDelay);
            transform.localPosition = new Vector3(0, 0, 0);
        }
        else
        {
            Jump(timer.Elapsed);
        }
    }

    /**
     * Calculates and sets the new y position according to the given time
     * 
     * @param time      The time
     */
    private void Jump(float time)
    {
        // x = v * t + 1/2 * a * t^2 where a = -9.8 (gravity)
        float newYPos = (velocity * time) + (0.5f * (9.8f * -1f) * (time * time));

        transform.localPosition = new Vector3(0, newYPos, 0);
    }
}
