﻿using UnityEngine;
using System.Collections;

public class CharacterEvent
{
    /**
     * Constructor: Creates a character event
     * 
     * @param time      The time of the event
     * @param type      The type of the even
     * @param wp        The location the player is heading to
     */
    public CharacterEvent(float time, EventType type, GameObject wp)
    {
        CreateEvent(time, type, wp);
    }

   /**
     * Constructor: Sets the character event variables
     * 
     * @param time          The time of the event
     * @param type          The type of the even
     * @param wp            The location the player is heading to
     * @param startingPos   The location of the player at the event
     */
    public CharacterEvent(float time, EventType type, GameObject wp, Vector3 startingPos)
        :this(time, type, wp)
    {
        location = startingPos;
    }

    public enum EventType
    {
        Movement,
        Interaction,
        Start,
        End
    }

    public float timeStamp;         // The time of the event
    public EventType eventType;     // The type of event
    public GameObject waypoint;     // The waypoint the player is heading to
    private Vector3 location;       // The location the player is heading to

    /**
     * Constructor: Sets the character event variables
     * 
     * @param time      The time of the event
     * @param type      The type of the even
     * @param wp        The location the player is heading to
     */
    public void CreateEvent(float time, EventType type, GameObject wp)
    {
        timeStamp = time;
        eventType = type;
        waypoint = wp;
        location = new Vector3(wp.transform.position.x, wp.transform.position.y, wp.transform.position.z);
    }

    /**
     * The time of the event
     * 
     * @param timeStamp
     * @return timeStamp
     */
    public float Time
    {
        get { return timeStamp; }
        set { timeStamp = value; }
    }

    /**
     * The type of the event
     * 
     * @param eventType
     * @return eventType
     */
    public EventType Type
    {
        get { return eventType; }
        set { eventType = value; }
    }

    /**
     * The locaction the character moved to in the event
     * 
     * @param location
     * @return location
     */
    public Vector3 Location
    {
        get { return location; }
        set { location = value; }
    }

    /**
     * The waypoint the character moved to in the event
     * 
     * @param waypoint
     * @return waypoint
     */
    public GameObject WayPoint
    {
        get { return waypoint; }
        set { 
            waypoint = value;
            location = new Vector3(waypoint.transform.position.x, waypoint.transform.position.y, waypoint.transform.position.z);
        }
    }

    public static implicit operator string(CharacterEvent characterEvent)
    {
        string result = "Time: " + characterEvent.Time + " Type: " + characterEvent.Type + " Coordiates: " + characterEvent.Location + " Waypoint: ";

        if (characterEvent.WayPoint)
            result += characterEvent.WayPoint.name;
        else
            result += "null";

        return result;
    }
}
