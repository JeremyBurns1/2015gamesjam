﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour 
{
    public Texture menuImage;               // The start screen image
    public AudioClip menuMusic;             // The menu music

    /**
     * Initializer: Starts playing the menu music if there is any
     */
    void Start()
    {
        if (menuMusic)
            AudioSource.PlayClipAtPoint(menuMusic, transform.position);
    }

    /**
     * Checks if enter has been pressed and if it has starts the game
     */
	void Update () 
    {
        if (Input.GetButtonUp("Enter"))
            Application.LoadLevel("FinalScene");

        // TODO:: Add a quit button
	}

    /**
     * Draws the start screen image if it exists
     */
    void OnGUI()
    {
        if (menuImage)
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), menuImage, ScaleMode.ScaleToFit, true, 0);
    }
}
