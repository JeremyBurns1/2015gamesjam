﻿using UnityEngine;
using System.Collections;

public class ArrowInput : MonoBehaviour 
{
    public bool debug = false;              // If the debug information is shown
    public Sprite sprite;                   // The arrows normal sprite
    public Sprite selectedSprite;           // The arrows selected sprite
    public SpriteRenderer spriteRenderer;   // The sprite renderer

    private DirectionIndicator indicator;   // The direction indicatior attached to the parent
    private WayPoint waypoint;              // The waypoint assosiated with the arrow   

    /**
     * Initializer: Resets the ArrowInput
     */
    void Awake () 
    {
        Reset();

        if (!spriteRenderer) spriteRenderer = GetComponent<SpriteRenderer>();

        if (!sprite) sprite = spriteRenderer.sprite;

        if (debug && selectedSprite) Debug.Log("Attach a selected sprite to the ArrowInput script attached to " + name);
	}

    /**
     * Mouse release event. Moves the player to the selected waypoint
     */
    void OnMouseUp()
    {
        if (debug) Debug.Log("Mouse click on indicator arrow");

        if (waypoint && indicator)
        { 
            indicator.MoveToSelectedWaypoint(waypoint); 
        }
        else if (debug)
        {
            string output = "";

            if (!waypoint && !indicator) output = "WayPoint and a DirectionIndicator";
            else if (!waypoint) output = "WayPoint";
            else if (!indicator) output = "DirectionIndicator";
 
            Debug.Log("A " + output + " isn't set on the ArrowInput script attached to " + name + ". These objects need to be set to excute the mouse event");
        }
    }

    /**
     * Mouse enter event.
     */
    void OnMouseEnter()
    {
        // TODO: Use a particle effect to show that the arrow has been hovered
        if (debug) Debug.Log("Mouse hovered over a indicator arrow");
        Select();
    }

    /**
     * Mouse exit event.
     */
    void OnMouseExit()
    {
        // TODO: Use a particle effect to show that the arrow has been hovered
        if (debug) Debug.Log("Mouse hovered over a indicator arrow");
        Deselect();
    }

    /**
     * Resets the arrow input. Set the arrow input to null so that the ArrowInput wont respond to mouse events to invalid waypoints.
     */
    public void Reset()
    {
        waypoint = null;
        Deselect();
    }

    /**
     * Selects the arrow. Updates to the selected sprite if there is one provided.
     */
    public void Select()
    {
        if (selectedSprite)
            spriteRenderer.sprite = selectedSprite;
    }

    /**
     * Deselects the arrow. Updates to the previous sprite if a selected sprite was provided.
     */
    public void Deselect()
    {
        if (selectedSprite)
            spriteRenderer.sprite = sprite;
    }

    /**
     * The waypoint associated with the arrow
     * 
     * @param   waypoint    
     * @return  waypoint
     */
    public WayPoint WayPoint
    {
        get { return waypoint; }
        set { waypoint = value; }
    }

    /**
     * The direction indicator of the parent player
     * 
     * @param   indicator
     */
    public DirectionIndicator Indicator
    {
        set { indicator = value; }
    }
}
