﻿using UnityEngine;
using System.Collections;

public class GameStats : MonoBehaviour 
{
    public static float runThroughLength = 30f;     // The run through length

    private static float totalRunThroughTime = 0f;  // The time of the entire play thorugh
    private static int drunkenessLevel = 0;         // The drunkeness level
    private static int  runThroughNumber = 0;       // The run through number

    private static Timer timer;                     // Times the current run through

    private static bool gameOver;                   // If the game is running

    /**
     * Initializer: Sets the start up time
     */
	void Awake() {
        timer = new Timer(runThroughLength);
	}

    /**
     * Resets thr time and increases the runthrough number
     */
    public static void ResetTime()
    {
        totalRunThroughTime += timer.Elapsed;
        timer.ResetTimer();
        ++runThroughNumber;
    }

    /**
     * Increases the drunkeness
     */
    public static void IncreaseDurnkeness()
    {
        ++drunkenessLevel;
    }

    /**
     * Pauses the time
     */
    public static void PauseTime()
    {
        timer.Pause();
    }

    /**
     * Returns the current time
     * 
     * @return currentTime      The current run through time
     */
    public static float CurrentTime
    {
        get { return timer.Elapsed; }
    }

    /**
     * Returns the run through time length
     * 
     * @return totalRunThroughTime       The total time allowed for a runthrough
     */
    public static float TotalTime
    {
        get { return totalRunThroughTime; }
    }

    /**
     * Returns the drunkeness level
     * 
     * @return drunkenessLevel       The drunkeness level
     */
    public static int DrunkenessLevel
    {
        get { return drunkenessLevel; }
    }

    /**
     * Returns the run through number
     * 
     * @return runThroughNumber       The run though number
     */
    public static int RunThroughNumber
    {
        get { return runThroughNumber; }
    }

    /**
     * Gets and sets the game over flag
     * 
     * @return gameOver       If the game is over or not
     */
    public static bool GameOver
    {
        get { return gameOver; }
        set { gameOver = value; }
    }
}
