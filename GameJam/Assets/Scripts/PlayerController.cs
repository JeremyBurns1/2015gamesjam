﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CharacterMovement))]
[RequireComponent(typeof(RecordedPlayerMovement))]
[RequireComponent(typeof(DeathTimer))]

public class PlayerController : MonoBehaviour 
{
    public bool debug = false;                  // If debug information is displayed

	public AudioClip[] footstepAudio;           // The audio played when the player moves

    private CharacterMovement movement;         // The character movement script
    private GameObject currentPosition;         // The current position
    private GameObject previousPosition;        // The previous position 

    private Vector3 selectedPath;               // The selected path
    private WayPoint wayPoint;                  // The current waypoint script
    private RecordedPlayerMovement movementTracking;    // Tracks the players movement
    private DirectionIndicator indicator;       // Indicates the player heading

    [HideInInspector]
    public bool drinking = false;               // If the player is drinking
    [HideInInspector]
    public bool timeJump = false;               // If the player has used a time jump
    [HideInInspector]
    public bool fatalhit = false;               // If the player has run to a previous player
    [HideInInspector]
    public bool disableCollider = true;         // The player collider will be enabled when a start event is created

    /**
     * Initializer: Sets variables 
     */
	void Start () 
    {
        movement = GetComponent<CharacterMovement>();
        movementTracking = GetComponent<RecordedPlayerMovement>();

        currentPosition = movement.startingWaypoint;
        previousPosition = currentPosition;
        movementTracking.AddStartEvent(currentPosition, currentPosition.transform.position);
        wayPoint = currentPosition.GetComponent<WayPoint>();
        selectedPath = wayPoint.CurrentPath.transform.position;

        if (!indicator) indicator = GetComponent<DirectionIndicator>();

        if (debug) Debug.Log("Player controller created");
	}

    /**
     * If the player input is active and is not moving, checks for player input and acts on it
     */
	void Update () 
    {
        // TODO: Maybe let the player change their movement once they have started moving
        if (!movement.IsMoving)
        {
            if (Input.GetButtonUp("Space"))
            {
                MoveToWaypoint(wayPoint.CurrentPath.GetComponent<WayPoint>());
            }
            else
            {
                if (Input.GetButtonUp("Left"))
                {
                    indicator.SelectWaypoint(wayPoint.SelectPreviousPath().GetComponent<WayPoint>());
                }

                if (Input.GetButtonUp("Right"))
                {
                    indicator.SelectWaypoint(wayPoint.SelectNextPath().GetComponent<WayPoint>());
                }

                indicator.DrawArrowPaths(wayPoint.WaypointPaths, wayPoint.transform.localPosition);
            }
        }
        if (Input.GetButtonUp("Enter"))
        {
            timeJump = true;
            if (debug) Debug.Log("Active player pressed enter");
        }
	}

    public void MoveToWaypoint(WayPoint newWaypoint)
    {
        previousPosition = currentPosition;
        currentPosition = newWaypoint.gameObject;
        wayPoint = currentPosition.GetComponent<WayPoint>();
        movementTracking.AddMoveEvent(currentPosition);
        movement.MoveTo(currentPosition.transform.position);
        if (debug) Debug.Log("Player is heading to " + currentPosition.name);

        // Play footsteps audio when space is pressed
        if (footstepAudio.Length > 0)
        {
            int selected = UnityEngine.Random.Range(0, footstepAudio.Length - 1); // Select random sound to play
            gameObject.GetComponent<AudioSource>().PlayOneShot(footstepAudio[selected]); // play selected sound from array.
        }

        // Hide all of the arrows
         if (debug) Debug.Log("Move to waypoint reset");
        indicator.HideArrows();
    }

    /**
     * Triggers the players death
     * 
     * @param timeDelay     How long until the player dies
     */
    public void Death(float timeDelay)
    {
        GetComponent<DeathTimer>().StartDeathTimer(timeDelay);
    }

    /**
     * Returns the players current position
     * 
     * @return currentPosition      The players current position
     */
    public GameObject CurrentPosition
    {
        get { return currentPosition; }
        set
        {
            currentPosition = value;
            wayPoint = currentPosition.GetComponent<WayPoint>();
        }
    }

    /**
     * Checks what the player collided with and acts on it
     * 
     * @param other     The thing the player collided with
     */
    void OnTriggerEnter(Collider other)
    {
        if (!disableCollider && other.tag.Contains("Player"))
        {
            if (debug) Debug.Log("Collisison " + name + " poistion: " + transform.position);
            fatalhit = true;
        }
        if (!disableCollider && other.tag == "Jock")
        {
            if (debug) Debug.Log("Collisison " + name + " poistion: " + transform.position);
            drinking = true;
        }
    }

    /**
     * Converts the given vector to one that appears 'behind' the player
     * 
     * @param original      The original vector
     * @return Vector3      The new vector
     * 
     */
    private Vector3 PlaceBehindPlayer(Vector3 original)
    {
        return new Vector3(original.x, original.y - 1.0f, original.z);
    }
}
