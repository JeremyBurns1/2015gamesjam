﻿using UnityEngine;
using System.Collections;

/**
 * SpriteSwitch
 * 
 * Switches out the main character sprite with an aternative at a specified run through number
 */
public class SpriteSwitch : MonoBehaviour 
{
    public Sprite alternativeSprite;        // The alternative sprite
    public int runNumberSwitch;             // The run time number the alternative switch is performed
    public SpriteRenderer spriteRenderer;   // The sprite renderer

    private bool switched = false;

    /**
     * Initializer: Sets the sprite renderer if it isn't set
     */
    void Start()
    {
        if (!spriteRenderer)
            spriteRenderer = GetComponent<SpriteRenderer>();
    }

    /**
     * Checks if the sprite renderer has been switched or if it needs to
     */
	void Update () 
    {
        if (!switched && runNumberSwitch <= GameStats.RunThroughNumber)
            SwitchSprite();
	}

    private void SwitchSprite()
    {
        if (!spriteRenderer)
        {
            Debug.LogError("Attach sprite renderer to " + name);
        }
        else if (!alternativeSprite)
        {
            Debug.LogError("Attach a sprite to the SpriteSwitch script attached to " + name);
        }
        else
        {
            spriteRenderer.sprite = alternativeSprite;
        }

        switched = true;
    }
}
