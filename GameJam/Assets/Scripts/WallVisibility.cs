﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SpriteRenderer))]
public class WallVisibility : MonoBehaviour 
{
    public SpriteRenderer renderer;        // The wall sprite renderer

    /**
     * Initializer: Assigns the variables
     */
	void Start () 
    {
        if (!renderer)
            renderer = GetComponent<SpriteRenderer>();
	}

    /**
     * If a player enters the trigger then diable the renderer
     * 
     * @param other     The object that entered the trigger
     */
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            renderer.enabled = false;
        }
    }

    /**
     * If a player is in the trigger and the renderer is enabled then disable it
     * 
     * @param other     The object that entered the trigger
     */
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && renderer.enabled)
        {
            renderer.enabled = false;
        }
    }

    /**
     * If a player exits the trigger then enable the renderer
     * 
     * @param other     The object that entered the trigger
     */
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            renderer.enabled = true;
        }
    }
}
