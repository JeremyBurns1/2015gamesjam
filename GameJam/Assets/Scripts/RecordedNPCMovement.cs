﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

/**
 * Class RecordedNPCMovement
 * 
 * Generates a random list of character events for NPC movement
 */
public class RecordedNPCMovement : MonoBehaviour 
{
    private List<CharacterEvent> events;    // The list of character events
    private CharacterMovement movement;     // The character movement script

    [HideInInspector]
    public int index = 0;                   // The index determining what event will be executed next

    /**
     * Initializer: Sets variables and randomly creates the NPC movement
     */
    void Awake()
    {
        events = new List<CharacterEvent>();

        movement = GetComponent<CharacterMovement>();
        GameObject startingLocation = movement.startingWaypoint;
        WayPoint waypoint = startingLocation.GetComponent<WayPoint>();

        // Randomly creates the NPC character events
        if (startingLocation && waypoint)
        {
            events.Add(new CharacterEvent(0f, CharacterEvent.EventType.Start, startingLocation));

            float time = 0f;

            for (int i = 0; i < 6; i++)
            {
                // TODO:: Randomise direction
                events.Add(new CharacterEvent(time, CharacterEvent.EventType.Movement, waypoint.SelectPreviousPath()));

                waypoint = waypoint.CurrentPath.GetComponent<WayPoint>();

                time += UnityEngine.Random.Range(3, 8);
            }
        }
    }

    /**
     * Executes the character event
     */
    public void ExecuteEvent()
    {
        if (index < events.Count)
        {
            CharacterEvent characterEvent = events[index];
            switch (characterEvent.Type)
            {
                case CharacterEvent.EventType.Movement:
                    ExecuteMoveEvent(characterEvent);
                    break;
                case CharacterEvent.EventType.Interaction:
                    ExecuteInteractionEvent(characterEvent);
                    break;
                case CharacterEvent.EventType.Start:
                    ExecuteStartEvent(characterEvent);
                    break;
                case CharacterEvent.EventType.End:
                    ExecuteEndEvent(characterEvent);
                    break;
            }
            ++index;
        }
    }

    /**
     * Gets a NPC to move to the position stores in the event
     * 
     * @param characterEvent    The character move event
     */
    private void ExecuteMoveEvent(CharacterEvent characterEvent)
    {
        CharacterMovement movement = gameObject.GetComponent<CharacterMovement>();
        movement.MoveTo(characterEvent.Location);
    }

    /**
     * Executes a character interaction event. Currently does nothing
     * 
     * @param characterEvent    The character interaction event
     */
    private void ExecuteInteractionEvent(CharacterEvent characterEvent)
    { }

    /**
     * Moves a NPC to the position stored in the event
     * 
     * @param characterEvent    The character start event
     */
    private void ExecuteStartEvent(CharacterEvent characterEvent)
    {
        transform.position = characterEvent.Location;
    }

    /**
     * Moves a NPC to the position stored in the event
     * 
     * @param characterEvent    The character end event
     */
    private void ExecuteEndEvent(CharacterEvent characterEvent)
    {
        transform.position = characterEvent.Location;
    }

    /**
     * Gets the character events
     * 
     * @return events    The list of character event
     */
    public List<CharacterEvent> Events
    {
        get { return events; }
    }
}
