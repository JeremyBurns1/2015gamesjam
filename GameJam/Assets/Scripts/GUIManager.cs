﻿using UnityEngine;
using System.Collections;
using System;

/**
 * GUIManager
 * 
 * The main GUI, this script needs to be attached to the main camera
 */
public class GUIManager : MonoBehaviour 
{
    public Master master;               // The master script
    public TimeWarpFlash flash;         // The time warp flash script

    public bool displayUI = true;       // If the UI is displayed or not
    public GUISkin guiSkin;				//assign the skin for GUI display

    public Texture viewport;
    public Texture topMenuForeground;
    public Texture topMenuBackground;
    public Texture bar;

    public Texture soloCup;
    public Texture vodkaBottle;
    public Texture nutBowl;

    public Texture number0;
    public Texture number1;
    public Texture number2;
    public Texture number3;
    public Texture number4;
    public Texture number5;
    public Texture number6;
    public Texture number7;
    public Texture number8;
    public Texture number9;

    public Texture[] morbidEndgameQuotes;

    private Texture quote;

    private int drunkLevel = 0;
    private int numberOfResets = 0;
    private int totalTime = 1050;
    private int remainingTime = 1050;

    void Start()
    {
        if (!master)
            Debug.LogError("Attach the master scrip to the GUIManager");

        if (!flash)
            flash = GetComponent<TimeWarpFlash>();

        if (morbidEndgameQuotes.Length > 0)
        {
            quote = morbidEndgameQuotes[UnityEngine.Random.Range(0, morbidEndgameQuotes.Length - 1)];
        }
    }

    void OnGUI()
    {
        numberOfResets = GameStats.RunThroughNumber;
        remainingTime = Convert.ToInt32((ScaleToWidth(totalTime) * GameStats.CurrentTime) / GameStats.runThroughLength);
        drunkLevel = GameStats.DrunkenessLevel;

        if (displayUI)
        {
            if (GameStats.GameOver && !master.currentPlayer && quote) 
                GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), quote, ScaleMode.ScaleToFit, true, 0);

            flash.DrawFlash();
            DisplayViewPort();
            DisplayTopMenu();
            DisplayBottomMenu();
            DisplayBottles();
            DisplayNumber();
        }
    }

    private void DisplayViewPort()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), viewport, ScaleMode.ScaleToFit, true, 0);
    }

    private void DisplayTopMenu()
    {
        int width = Screen.width;
        int height = ScaleToHeight(topMenuBackground.height);

        GUI.BeginGroup(new Rect(0, 0, width, height));
        GUI.BeginGroup(new Rect(0, 0, ScaleToWidth(topMenuBackground.width) - remainingTime, height));
        GUI.DrawTexture(new Rect(0, 0, ScaleToWidth(topMenuBackground.width), height), topMenuBackground, ScaleMode.ScaleToFit, true, 0);
        GUI.EndGroup();
        GUI.DrawTexture(new Rect(0, 0, width, height), topMenuForeground, ScaleMode.ScaleToFit, true, 0);
        GUI.EndGroup();
    }

    private void DisplayNumber()
    {
        int x = ScaleToWidth(1713);
        int y = ScaleToHeight(25);

        Texture number;
        if (numberOfResets == 0) number = number0;
        else if (numberOfResets == 1) number = number1;
        else if (numberOfResets == 2) number = number2;
        else if (numberOfResets == 3) number = number3;
        else if (numberOfResets == 4) number = number4;
        else if (numberOfResets == 5) number = number5;
        else if (numberOfResets == 6) number = number6;
        else if (numberOfResets == 7) number = number7;
        else if (numberOfResets == 8) number = number8;
        else number = number9;

        GUI.DrawTexture(new Rect(x, y, ScaleToWidth(number.width), ScaleToHeight(number.height)), number, ScaleMode.ScaleToFit, true, 0);
    }

    private void DisplayBottomMenu()
    {
        GUI.BeginGroup(new Rect(0, Screen.height - ScaleToHeight(bar.height), Screen.width, ScaleToHeight(bar.height)));
        GUI.DrawTexture(new Rect(0, 0, Screen.width, ScaleToHeight(bar.height)), bar, ScaleMode.ScaleToFit, true, 0);
        GUI.EndGroup();
    }

    private void DisplayBottles()
    {
        int cup1X = ScaleToWidth(140);
        int cup1Y = ScaleToHeight(845);
        int cup2X = ScaleToWidth(500);
        int cup2Y = ScaleToHeight(830);
        int cup3X = ScaleToWidth(676);
        int cup3Y = ScaleToHeight(800); // Draw first
        int cup4X = ScaleToWidth(787);
        int cup4Y = ScaleToHeight(860);
        int cup5X = ScaleToWidth(1004);
        int cup5Y = ScaleToHeight(820);
        int cup6X = ScaleToWidth(1200);
        int cup6Y = ScaleToHeight(842);
        int bottle1X = ScaleToWidth(325);
        int bottle1Y = ScaleToHeight(621);
        int bottle2X = ScaleToWidth(915); // Draw first
        int bottle2Y = ScaleToHeight(581);
        int bowlX = ScaleToWidth(1495);
        int bowlY = ScaleToHeight(899);

        int cupWidth = ScaleToWidth(soloCup.width / 2);
        int cupHeight = ScaleToWidth(soloCup.height / 2);
        int bottleWidth = ScaleToWidth(vodkaBottle.width / 3 * 2);
        int bottleHeight = ScaleToWidth(vodkaBottle.height / 3 * 2);

        // Draw the back ones first

        if (drunkLevel > 3) GUI.DrawTexture(new Rect(cup3X, cup3Y, cupWidth, cupHeight), soloCup, ScaleMode.ScaleToFit, true, 0);
        if (drunkLevel > 7) GUI.DrawTexture(new Rect(bottle2X, bottle2Y, bottleWidth, bottleHeight), vodkaBottle, ScaleMode.ScaleToFit, true, 0);

        if (drunkLevel > 0) GUI.DrawTexture(new Rect(cup1X, cup1Y, cupWidth, cupHeight), soloCup, ScaleMode.ScaleToFit, true, 0);
        if (drunkLevel > 1) GUI.DrawTexture(new Rect(cup2X, cup2Y, cupWidth, cupHeight), soloCup, ScaleMode.ScaleToFit, true, 0);
        if (drunkLevel > 4) GUI.DrawTexture(new Rect(cup4X, cup4Y, cupWidth, cupHeight), soloCup, ScaleMode.ScaleToFit, true, 0);
        if (drunkLevel > 5) GUI.DrawTexture(new Rect(cup5X, cup5Y, cupWidth, cupHeight), soloCup, ScaleMode.ScaleToFit, true, 0);
        if (drunkLevel > 6) GUI.DrawTexture(new Rect(cup6X, cup6Y, cupWidth, cupHeight), soloCup, ScaleMode.ScaleToFit, true, 0);

        if (drunkLevel > 2) GUI.DrawTexture(new Rect(bottle1X, bottle1Y, bottleWidth, bottleHeight), vodkaBottle, ScaleMode.ScaleToFit, true, 0);

        GUI.DrawTexture(new Rect(bowlX, bowlY, ScaleToWidth(nutBowl.width / 3 * 2), ScaleToWidth(nutBowl.height / 3 * 2)), nutBowl, ScaleMode.ScaleToFit, true, 0);
    }

    private int ScaleToWidth(int width)
    {
        return Convert.ToInt32((Screen.width * width) / viewport.width);
    }

    private int ScaleToHeight(int height)
    {
        return Convert.ToInt32((Screen.height * height) / viewport.height);
    }
}
