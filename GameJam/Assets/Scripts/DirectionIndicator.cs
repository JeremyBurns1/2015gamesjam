﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class DirectionIndicator : MonoBehaviour 
{
    public bool debug = false;              // If the debug information is shown
    public GameObject directionArrow;       // The direction arrow prefab
    public PlayerController controller;     // The player controller

    private List<ArrowInput> arrows;        // The list of the attached arrows
    private bool arrowsHidden = true;       // If the arrows are currently hidden or not

    /**
     * Initializer: Create the arrow input list and checks that the requried components are attached
     */
	void Start () 
    {
        arrows = new List<ArrowInput>();

        if (!directionArrow)
            Debug.LogError("Attach a direction arrow game object to the DirectionIndicator script on " + name);
        if (!controller)
            controller = GetComponent<PlayerController>();
        if (debug)
            Debug.Log("DirectionIndicator created");
	}

    /**
     * Draws all of the paths (arrows) of the current waypoint.
     * 
     * @param waypointPaths     The waypoint paths
     * @param currentPos        The players current position
     */
    public void DrawArrowPaths(GameObject[] waypointPaths, Vector3 currentPos)
    {
        if (arrowsHidden)
        {
            // Add any missing sprite renders
            while (waypointPaths.Length > arrows.Count)
            {
                CreateArrow();
            }

            // Disable all of the sprite renderers so that they are not shown
            HideArrows();

            // Draw all of the sprite renders required
            if (waypointPaths.Length > 0)
            {
                for (int i = 0; i < waypointPaths.Length; ++i)
                {
                    Vector3 direction = (waypointPaths[i].transform.localPosition - currentPos).normalized;
                    float zAngleRadians = Mathf.Acos(direction.z / direction.magnitude);
                    float zAngleDegrees = zAngleRadians * (180 / Mathf.PI);

                    if (!float.IsNaN(zAngleDegrees))
                    {
                        // Fix the angles depening on if the x direction is negtive or not
                        // For rotation the degrees value needs to be modified
                        // For position the radians value needs to be modified
                        if (direction.x > 0)
                        {
                            zAngleDegrees *= -1;
                        }
                        else
                        {
                            zAngleRadians *= -1;
                        }

                        Vector3 newRotation = new Vector3(90, 0, zAngleDegrees);
                        arrows[i].transform.eulerAngles = newRotation;
                    }

                    // Calculate a 2D vector that is in the direction the arrow needs to move in 
                    float x = Mathf.Sin(zAngleRadians);
                    float y = Mathf.Cos(zAngleRadians);
                    Vector2 position = new Vector2(x, y);

                    // Move the arrow away from the player by making the magnitude of the vector larger
                    position = position * 1.5f;

                    // Set the new value of the vector and enable its sprite renderer
                    // The position needs to be set in local space since the position vectors have been calulated from the center of the player
                    // The new z position is set to 0.1 so that the arrows appear behind the player
                    arrows[i].transform.localPosition = new Vector3(position.x, position.y, 0.1f);
                    arrows[i].GetComponent<SpriteRenderer>().enabled = true;

                    // Set the waypoint
                    arrows[i].WayPoint = waypointPaths[i].GetComponent<WayPoint>();
                }
                arrowsHidden = false;
            }
        }
    }

    /**
     * Hides and resets all of the atttached arrows
     */
    public void HideArrows()
    {
        if (!arrowsHidden)
        {
            foreach (ArrowInput arrow in arrows)
            {
                arrow.Reset();
                SpriteRenderer renderer = arrow.GetComponent<SpriteRenderer>();
                if (renderer)
                {
                    renderer.enabled = false;
                }
            }
            arrowsHidden = true;
        }
    }

    /**
     * Gets the player controller to move to the selected waypoint.
     */
    public void MoveToSelectedWaypoint(WayPoint selectedWaypoint)
    {
        controller.MoveToWaypoint(selectedWaypoint);
    }

    /**
     * Selects the arrow assosiated to the selected waypoint and deselects all other arrows.
     * 
     * @param selectedWaypoint      The selected waypoint.
     */
    public void SelectWaypoint(WayPoint selectedWaypoint)
    {
        foreach (ArrowInput arrow in arrows)
        {
            if (arrow.WayPoint == selectedWaypoint)
            {
                arrow.Select();
            }
            else
            {
                arrow.Deselect();
            }
        }
    }

    /**
     * Creates a new direction arrow.
     */
    private void CreateArrow()
    {
        directionArrow.transform.position = transform.position;
        GameObject childObject = Instantiate(directionArrow);
        childObject.transform.parent = gameObject.transform;

        ArrowInput arrow = childObject.GetComponent<ArrowInput>();
        arrow.Indicator = this;
        arrows.Add(arrow);
        
        if (debug) Debug.Log("Created direction indicator " + arrows.Count);
        arrowsHidden = false;
    }
}
