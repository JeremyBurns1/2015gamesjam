﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

/**
 * Class MusicController
 * 
 * Switches the music track depending on the current run through number
 */
public class MusicController : MonoBehaviour 
{
    public AudioClip[] audioFiles;      // The audio files

    private Dictionary<int, AudioClip> musicFiles;  // A map that connects the music files to the run through number
	private AudioSource audioSource;    // The audio source
    private int runThroughNumber;       // The current run through number

    /**
     * Initializer: Creates the music files map and starts playing the music
     */
	void Start () 
	{
        musicFiles = new Dictionary<int,AudioClip>();
        int i = 0;

        foreach (AudioClip file in audioFiles)
        {
            musicFiles.Add(i,file);
            ++i;
        }

        audioSource = gameObject.GetComponent<AudioSource>();
        if (audioSource && audioFiles[0])
        {
            audioSource.clip = musicFiles[0];
            audioSource.Play();
        }

	}

    /**
     * Checks if the run through number has changes and updates the music if it has
     */
	void Update () 
	{
        if (audioSource.isPlaying)
        {
            if (GameStats.RunThroughNumber != runThroughNumber)
            {
                runThroughNumber = GameStats.RunThroughNumber;
                UpdateAudio();
            }

            // Kill the music if the player dies
            if (GameStats.GameOver)
            {
                audioSource.Pause();
            }
        }
	}

    /**
     * Updates the currently playing music file depending on the run through number
     */
    private void UpdateAudio()
    {
        if (audioFiles[runThroughNumber])
        {
            float time = audioSource.time;
            audioSource.clip = audioFiles[runThroughNumber];
            audioSource.time = time;
            audioSource.Play();
            audioSource.loop = true;
        }
    }
}

