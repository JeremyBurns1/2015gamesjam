﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class Master : MonoBehaviour 
{
    public bool debug = false;              // If the debug information is displayed
    public GameObject currentPlayer;        // The active player
    public GameObject playerPrefab;         // The player prefab
    public GameObject mainCamera;           // The main camera

	public AudioClip timeWarp;              // The time warp audio

    public List<RecordedNPCMovement> nPCs;  // The list of the NPC's movement   
    public float minimumDrinkTime = 5f;     // The minimum time between the player drinks

    public bool increaseDrunkness = false;  // Manually increases the player drunkeness

    private List<RecordedPlayerMovement> previousPlayerPlaythorughs;    // A list of the previous player playthoughts
    private float startDrinking = 0f;       // When the player starts drinking
    private PlayerController currentPlayerContoller;    // The current players PlayerController script
    private TimeWarpFlash flash;

    /**
     * Initializer: Sets up variables
     */
	void Start () {
        previousPlayerPlaythorughs = new List<RecordedPlayerMovement>();
        currentPlayerContoller = currentPlayer.GetComponent<PlayerController>();

        startDrinking = 0 - minimumDrinkTime;

        if (!mainCamera) 
            Debug.LogError("Attach main camera to the master script");
        if (mainCamera && !flash)
            flash = mainCamera.GetComponent<TimeWarpFlash>();
	}

    /**
     * Executes all of the player and NPC move events at the correct time
     * Checks if the player is still alive or if they are drinking
     */
	void Update () 
    {
        MoveCharacters();

        // Check for drinking
        if (currentPlayerContoller.drinking == true)
        {
            if (minimumDrinkTime < GameStats.CurrentTime - startDrinking)
            {
                DrinkBitch();
            }
            currentPlayerContoller.drinking = false;
        }

        if (increaseDrunkness)
        {
            DrinkBitch();
            increaseDrunkness = false;
        }

        // Check for a player collisison (mass death)
        if (!GameStats.GameOver && currentPlayerContoller.fatalhit && GameStats.CurrentTime > 2.0f)
        {
            MassDeath();
            if (debug) Debug.Log("Player death triggered");
        }

        if (!GameStats.GameOver)
        {
            // Execute the user triggered time jump
            if (currentPlayerContoller.timeJump)
            {
                // Dont reset if it hasn't passed past a certain time
                if (GameStats.CurrentTime > 15.0) Reset();
                currentPlayerContoller.timeJump = false;
            }

            // Force reset if the player runs out of time
            if (GameStats.CurrentTime > GameStats.runThroughLength) Reset();
        }
        else
        {
            // If game over and the current player has been destroyed
            if (!currentPlayer && Input.GetButtonUp("Enter"))
                    Application.LoadLevel("Menu");
        }
	}

    /**
     * Resets the playthrough
     * 
     * Adds the player movement to the lis of player movements, diables the previous players input, resets the time and created a new active player
     */
    public void Reset()
    {
        if (debug) Debug.Log("Reset the playthrough");
        // TODO make sure that the flash times properly
        flash.StartFlash();

		if (timeWarp)
		{
			AudioSource.PlayClipAtPoint(timeWarp, currentPlayer.transform.position);
		}

        // Reset the timer and add to the run through amount
        GameStats.ResetTime();
        startDrinking = 0 - minimumDrinkTime;

        // Save the previous player movement and pass it to the newly created previous player
        RecordedPlayerMovement currentPlayerRecordedMovement = currentPlayer.GetComponent<RecordedPlayerMovement>();
        currentPlayerRecordedMovement.AddEndEvent();

        // Create the new previous player
        GameObject newPreviousPlayer = Instantiate(playerPrefab);

        // Assign the current runthrough events to the new player object
        RecordedPlayerMovement previousPlayerRecordedMovement = newPreviousPlayer.GetComponent<RecordedPlayerMovement>();
        previousPlayerRecordedMovement.SetEvents(currentPlayerRecordedMovement.Events);
        previousPlayerPlaythorughs.Add(previousPlayerRecordedMovement);

        // Reset the active players events
        currentPlayerRecordedMovement.Clear();
        currentPlayerRecordedMovement.AddStartEvent(currentPlayerContoller.CurrentPosition, transform.position);

        // Reset the player tracker indexes
        foreach (RecordedPlayerMovement previousPlayer in previousPlayerPlaythorughs)
        {
            previousPlayer.index = 0;
        }

        foreach (RecordedNPCMovement npc in nPCs)
        {
            npc.index = 0;
        }
    }

    /**
     * Moves all of the characters in the scene
     * Also checks if the player has died
     */
    public void MoveCharacters()
    {
        if (previousPlayerPlaythorughs.Count > 0)
        {
            foreach (RecordedPlayerMovement previousPlayer in previousPlayerPlaythorughs)
            {
                // Only execute the first event in the list
                if (previousPlayer && previousPlayer.Events.Count > 0 && previousPlayer.index < previousPlayer.Events.Count)
                {
                    if (previousPlayer.Events[previousPlayer.index].Time < GameStats.CurrentTime)
                    {
                        previousPlayer.ExecuteEvent();
                    }
                }
            }
        }

        // Move all of the NPC's
        foreach (RecordedNPCMovement npc in nPCs)
        {
            // Only execute the first event in the list
            if (npc && npc.Events.Count > 0 && npc.index < npc.Events.Count)
            {
                if (npc.Events[npc.index].Time < GameStats.CurrentTime)
                {
                    npc.ExecuteEvent();
                }
            }
        }
    }

    /**
     * Tiggers the death of all the previous players and the current player 
     */
    public void MassDeath()
    {
        GameStats.GameOver = true;
        GameStats.PauseTime();

        float timeOfDeath = 0.5f;

        CameraWobble wobble = mainCamera.GetComponent<CameraWobble>();
        
        foreach (RecordedPlayerMovement previousPlayer in previousPlayerPlaythorughs)
        {
            previousPlayer.GetComponent<DeathTimer>().StartDeathTimer(timeOfDeath);
            wobble.Invoke("StartShake", timeOfDeath);
            timeOfDeath += 1f + Random.Range(0f, 10.0f) / 10;
        }

        Destroy(currentPlayer.GetComponent<AudioListener>());
        mainCamera.GetComponent<AudioListener>().enabled = true;
        if (debug) Debug.Log("Main player death");
        currentPlayerContoller.Death(timeOfDeath);
    }

    /**
     * Increases the player drunkeness
     */
    public void DrinkBitch()
    {
        GameStats.IncreaseDurnkeness();
        startDrinking = GameStats.CurrentTime;
    }
}
