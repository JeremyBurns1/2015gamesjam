﻿using UnityEngine;
using System.Collections;

public class NPCAudio : MonoBehaviour 
{
    public AudioClip[] collisionAudio;      // The audio played when the NPC collides with a player

    private Timer timer;                    // Timer for the timer between audio

    void Awake()
    {
        timer = new Timer(0f);
    }

    /**
     * Playes audio if the NPC collides with a player
     * 
     * @param other     The collider the NPC collided with
     */
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && timer.TimeFinished())
        {
            timer.ResetTimer(5f);
            foreach (AudioClip clip in collisionAudio)
            {
                if (clip) AudioSource.PlayClipAtPoint(clip, transform.position);
            }
        }
    }
}
