﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

/**
 * Class RecordedPlayerMovement
 * 
 * Records the players actions through a playthrough and stores them to carried out through each time reset
 * The index records the movement that will be executed next
 */
public class RecordedPlayerMovement : MonoBehaviour 
{
    public bool debug = false;              // If the debug information is displayed
    public CharacterMovement movement;     // The character movement script
    private List<CharacterEvent> events;    // A list of actions the player performs

    [HideInInspector]
    public int index = 0;                   // The index determining what event will be executed next

    /**
     * Initalizer: Creates the character events list and sets the player script variables
     */
    void Awake() 
    {
        if (events == null)
            events = new List<CharacterEvent>();
        if (!movement)
            movement = GetComponent<CharacterMovement>();
    }

    /**
     * Adds a move event to the players event list
     * 
     * @param heading       The location the player is moving to
     */
    public void AddMoveEvent(GameObject heading)
    {
        if (debug) Debug.Log("Add new character move event to " + heading.name);
        events.Add(new CharacterEvent(GameStats.CurrentTime, CharacterEvent.EventType.Movement, heading));
    }

    /**
     * Adds a interaction event to the players event list
     */
    public void AddInteractionEvent()
    {
        // TODO: Implement a character interaction event
        events.Add(new CharacterEvent(GameStats.CurrentTime, CharacterEvent.EventType.Interaction, gameObject));
    }

    /**
     * Adds a start event to the player event list
     * 
     * @param startPos      The players start location
     */
    public void AddStartEvent(GameObject startHeading)
    {
        events.Add(new CharacterEvent(GameStats.CurrentTime, CharacterEvent.EventType.Start, startHeading));
    }

    /**
     * Adds a start event to the player event list
     * 
     * @param startPos      The players start heading
     * @param startPos      The players start position
     */
    public void AddStartEvent(GameObject startHeading, Vector3 startPos)
    {
        events.Add(new CharacterEvent(GameStats.CurrentTime, CharacterEvent.EventType.Start, startHeading, startPos));
    }

    /**
     * Adds an end event to player event list
     */
    public void AddEndEvent()
    {
        events.Add(new CharacterEvent(GameStats.CurrentTime, CharacterEvent.EventType.End, gameObject));
    }

    /**
     * Executes the character event
     */
    public void ExecuteEvent()
    {
        if (index < events.Count)
        {
            CharacterEvent characterEvent = events[index];
            switch (characterEvent.Type)
            {
                case CharacterEvent.EventType.Movement:
                    ExecuteMoveEvent(characterEvent);
                    break;
                case CharacterEvent.EventType.Interaction:
                    ExecuteInteractionEvent(characterEvent);
                    break;
                case CharacterEvent.EventType.Start:
                    ExecuteStartEvent(characterEvent);
                    break;
                case CharacterEvent.EventType.End:
                    ExecuteEndEvent(characterEvent);
                    break;
            }
            ++index;
        }
    }

    /**
     * Executes a character move event. Tells the character to move to a position.
     * 
     * @param characterEvent    The character event to execute
     */
    private void ExecuteMoveEvent(CharacterEvent characterEvent)
    {
        movement.MoveTo(characterEvent.Location);
    }

    /**
     * Executes a character inteaction event. Currently does nothing
     * 
     * @param characterEvent    The character event to execute
     */
    private void ExecuteInteractionEvent(CharacterEvent characterEvent)
    {
        // There is no interaction events yet
    }

    /**
     * Executes a character start event. Moves the character to a position and enables its collider
     * 
     * @param characterEvent    The character event to execute
     */
    private void ExecuteStartEvent(CharacterEvent characterEvent)
    {
        transform.position = characterEvent.Location;
    }

    /**
     * Executes a character end event. Moves the character off screen and disables its collider
     * 
     * @param characterEvent    The character event to execute
     */
    private void ExecuteEndEvent(CharacterEvent characterEvent)
    {
        transform.position = new Vector3(100, 100, 100);
    }

    public void Clear()
    {
        Debug.Log("Reset the character events for " + name);
        events = new List<CharacterEvent>();
    }

    /**
     * Gets the list of player events
     * 
     * @param events    The list of player events
     */
    public List<CharacterEvent> Events
    {
        get { return events; }
    }

    public void SetEvents(List<CharacterEvent> newEvents)
    {
        events = new List<CharacterEvent>();
        Debug.Log("Amount of new events being added " + newEvents.Count);
        events.AddRange(newEvents);
    }
}
