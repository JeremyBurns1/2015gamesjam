﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BlurEffect))]
[RequireComponent(typeof(GlowEffect))]
public class FollowPlayer : MonoBehaviour 
{
    public Master master;       // The master script

    public BlurEffect blur;    // The camera blur script
    public GlowEffect glow;    // The camera glow script

    public Color lowGlow = new Color(13, 5, 9, 0);              // The colour overlay on low glow
    public Color mediumGlow = new Color(78, 25, 52, 0);         // The colour overlay on medium glow
    public Color highGlow = new Color(176, 35, 108, 0);         // The colour overlay on high glow
    public Color veryHighGlow = new Color(238, 28, 138, 0);     // The colour overlay on very high glow

    private int drunkness;      // The player drunkeness level

    /**
     * Initializer: Sets the camera blur effect and glow effect scripts
     */
	void Start () 
    {
        if (!blur) blur = GetComponentInChildren<BlurEffect>();
        if (!glow) glow = GetComponentInChildren<GlowEffect>();

        if (!master)
            Debug.LogError("Attach master to the camera FollowPlayer");
	}

    /**
     * Updates the cameara position to follow the player and updates the blur and the glow if the drunkeness level changes
     */
	void Update () {
        if (master.currentPlayer)
        {
            transform.position = CameraPosition;
        }

        // Only update the variables in the camera overlays if they are different
        if (GameStats.DrunkenessLevel != drunkness)
        {
            drunkness = GameStats.DrunkenessLevel;

            blur.iterations = drunkness * 2;

            if (drunkness > 4 && !glow.enabled)
            {
                glow.enabled = true;
            }

            if (drunkness == 5) glow.glowTint = lowGlow;
            else if (drunkness == 6) glow.glowTint = mediumGlow;
            else if (drunkness == 7) glow.glowTint = highGlow;
            else if (drunkness > 7) glow.glowTint = veryHighGlow;
        }
	}

    public Vector3 CameraPosition
    {
        get { return new Vector3(master.currentPlayer.transform.position.x, transform.position.y, master.currentPlayer.transform.position.z); }
    }
}
